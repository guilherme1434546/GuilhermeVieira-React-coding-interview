import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';
import { InlineTextField } from './inlineTextField';

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  sx,
}) => {
  const onChange = (value: string) => {
    console.log(value);
  };

  const emailValidation = (value: string) => {
    if (!value.includes('@')) {
      return 'Email must include @';
    }
  };

  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2}>
          <InlineTextField value={name} onChange={onChange} />

          <InlineTextField value={email} onChange={onChange} />
        </Box>
      </Box>
    </Card>
  );
};
