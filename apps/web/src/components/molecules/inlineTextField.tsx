import { useEffect, useState, useRef } from 'react';
export interface IInlineTextFieldProps {
  value: string;
  onChange: (value: string) => void;
  validate?: (value: string) => string | undefined;
}

export const InlineTextField: React.FC<IInlineTextFieldProps> = ({
  value,
  onChange,
  validate,
}) => {
  const [editing, setEditing] = useState(false);
  const [text, setText] = useState(value);
  const inputRef = useRef(null);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setText(e.target.value);
  };

  const onKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      const validationError = validate(text);
      if (validationError) {
        alert(validationError);
      } else {
        setEditing(false);
        onChange(text);
      }
    }
  };

  // useEffect(() => {
  //   const eventListener = (e: MouseEvent) => {
  //     if (!inputRef.current?.contains(e.target)) {
  //       setEditing(false);
  //     }
  //   };

  //   document.addEventListener('click', eventListener);

  //   return () => document.removeEventListener('click', eventListener);
  // }, []);

  return editing ? (
    <input
      type="text"
      value={text}
      onChange={handleChange}
      onKeyDown={onKeyDown}
      ref={inputRef}
    />
  ) : (
    <div onClick={() => setEditing(true)}>{value}</div>
  );
};
